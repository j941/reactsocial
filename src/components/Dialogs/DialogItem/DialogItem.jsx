import React from 'react';
import { NavLink } from 'react-router-dom';
import s from "./../Dialogs.module.css"
import d from "./DialogItem.module.css";


const DialogItem = (props) => {
    let path = "/dialogs" + props.id;

    return <div className={s.dialog + ' ' + s.active}>
        <div className={d.item}>
            <img src='https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_DE/viacom-358518-Full-Image_GalleryBackground-de-DE-1501272675282._V7ea3c5f3b475ec3e9224ddb482756b00_SX1080_.jpg'></img>
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    </div>
}

export default DialogItem;